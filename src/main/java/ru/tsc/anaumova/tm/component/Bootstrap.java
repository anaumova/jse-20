package ru.tsc.anaumova.tm.component;

import ru.tsc.anaumova.tm.api.repository.ICommandRepository;
import ru.tsc.anaumova.tm.api.repository.IProjectRepository;
import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.api.repository.IUserRepository;
import ru.tsc.anaumova.tm.api.service.*;
import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.command.project.*;
import ru.tsc.anaumova.tm.command.system.*;
import ru.tsc.anaumova.tm.command.task.*;
import ru.tsc.anaumova.tm.command.user.*;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.system.UnknownArgumentException;
import ru.tsc.anaumova.tm.exception.system.UnknownCommandException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.repository.CommandRepository;
import ru.tsc.anaumova.tm.repository.ProjectRepository;
import ru.tsc.anaumova.tm.repository.TaskRepository;
import ru.tsc.anaumova.tm.repository.UserRepository;
import ru.tsc.anaumova.tm.service.*;
import ru.tsc.anaumova.tm.util.DateUtil;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateCommand());
        registry(new UserChangePasswordCommand());
    }

    private void initDemoData() {
        User usualUser = userService.create("user", "user", "user@user.ru");
        User adminUser = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(usualUser.getId(), "Project C", "15.11.2019").setStatus(Status.IN_PROGRESS);
        projectService.create(usualUser.getId(), "Project A", "13.05.2021").setStatus(Status.COMPLETED);
        projectService.create(usualUser.getId(), "Project D", "02.02.2020").setStatus(Status.IN_PROGRESS);
        projectService.create(adminUser.getId(), "Project B", "01.02.2019").setStatus(Status.NOT_STARTED);
        taskService.create(usualUser.getId(), "Task F", "05.10.2018").setStatus(Status.IN_PROGRESS);
        taskService.create(adminUser.getId(), "Task E", "11.04.2021").setStatus(Status.NOT_STARTED);
        taskService.create(adminUser.getId(), "Task G", "02.02.2021").setStatus(Status.NOT_STARTED);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **"))
        );
    }

    public void run(final String[] args) {
        if (processArgument(args)) new ExitCommand().execute();
        initLogger();
        initDemoData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
            }
        }
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new UnknownArgumentException(arg);
        abstractCommand.execute();
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}