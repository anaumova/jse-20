package ru.tsc.anaumova.tm.repository;

import ru.tsc.anaumova.tm.api.repository.ITaskRepository;
import ru.tsc.anaumova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null || task.getUserId() == null) continue;
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

}