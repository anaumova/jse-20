package ru.tsc.anaumova.tm.command;

import ru.tsc.anaumova.tm.api.model.ICommand;
import ru.tsc.anaumova.tm.api.service.IAuthService;
import ru.tsc.anaumova.tm.api.service.IServiceLocator;
import ru.tsc.anaumova.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public abstract void execute();

    public String getArgument() {
        return null;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}