package ru.tsc.anaumova.tm.command.project;

import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-show-by-index";

    public static final String DESCRIPTION = "Show project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}