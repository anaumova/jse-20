package ru.tsc.anaumova.tm.command.system;

import ru.tsc.anaumova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show command list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}