package ru.tsc.anaumova.tm.command.user;

import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-show-profile";

    public static final String DESCRIPTION = "Show user info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}