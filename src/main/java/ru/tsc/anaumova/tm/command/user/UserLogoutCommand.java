package ru.tsc.anaumova.tm.command.user;

import ru.tsc.anaumova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Log out";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}