package ru.tsc.anaumova.tm.command.task;

import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}