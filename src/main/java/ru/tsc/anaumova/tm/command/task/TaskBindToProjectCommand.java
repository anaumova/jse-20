package ru.tsc.anaumova.tm.command.task;

import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-bind-to-project";

    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}