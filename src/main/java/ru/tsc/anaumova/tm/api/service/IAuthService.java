package ru.tsc.anaumova.tm.api.service;

import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.model.User;

public interface IAuthService {

    void registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(Role[] roles);

}