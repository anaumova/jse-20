package ru.tsc.anaumova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}