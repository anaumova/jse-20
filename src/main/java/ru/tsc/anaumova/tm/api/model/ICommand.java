package ru.tsc.anaumova.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}