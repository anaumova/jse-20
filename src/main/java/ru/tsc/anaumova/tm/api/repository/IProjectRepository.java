package ru.tsc.anaumova.tm.api.repository;

import ru.tsc.anaumova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}