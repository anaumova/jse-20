package ru.tsc.anaumova.tm.model;

import java.util.UUID;

public abstract class AbstractModel {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}