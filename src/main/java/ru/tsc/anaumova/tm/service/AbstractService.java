package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.repository.IRepository;
import ru.tsc.anaumova.tm.api.service.IService;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.exception.entity.ModelNotFoundException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.IncorrectIndexException;
import ru.tsc.anaumova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M remove(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final M model = findOneById(id);
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        final M model = findOneByIndex(index);
        return remove(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}