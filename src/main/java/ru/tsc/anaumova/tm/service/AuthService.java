package ru.tsc.anaumova.tm.service;

import ru.tsc.anaumova.tm.api.service.IAuthService;
import ru.tsc.anaumova.tm.api.service.IUserService;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.exception.field.EmptyLoginException;
import ru.tsc.anaumova.tm.exception.field.EmptyPasswordException;
import ru.tsc.anaumova.tm.exception.system.AccessDeniedException;
import ru.tsc.anaumova.tm.exception.system.IncorrectLoginOrPasswordException;
import ru.tsc.anaumova.tm.exception.system.PermissionException;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        User user = userService.findByLogin(login);
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}